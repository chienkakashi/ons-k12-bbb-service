<?php

namespace App\Services;

use BigBlueButton\BigBlueButton;
use BigBlueButton\Parameters\CreateMeetingParameters;
use BigBlueButton\Parameters\JoinMeetingParameters;
use Exception;

class BBBService
{
    private BigBlueButton $bbb;

    public function __construct()
    {
        $this->bbb = new BigBlueButton();
    }

    /**
     * Create the meeting
     * @param string $meetingName
     *
     * @return array
     */
    public function createMeeting(string $meetingName): array
    {
        $meetingId         = time();
        $passwordModerator = rand();
        $passwordAttendee  = rand();
        $message           = 'success';
        $code              = 200;
        $data              = [];

        try {
            $createParams = new CreateMeetingParameters($meetingId, $meetingName);
            $createParams = $createParams->setModeratorPassword($passwordModerator)
                                         ->setAttendeePassword($passwordAttendee);

            $createMeetingResponse = $this->bbb->createMeeting($createParams);
            $data                  = $createMeetingResponse->getRawXml();
        }
        catch (Exception $e) {
            $message = $e->getMessage();
            $code    = $e->getCode();
        }

        return compact('message', 'code', 'data');
    }

    /**
     * Get link join meeting
     * @param string $meetingId
     * @param string $username
     * @param string $password
     *
     * @return string
     */
    public function getJoinMeetingURL(string $meetingId, string $username, string $password): string
    {
        $joinParams = new JoinMeetingParameters($meetingId, $username, $password);
        $joinParams->setRedirect(true);

        return $this->bbb->getJoinMeetingURL($joinParams);
    }
}