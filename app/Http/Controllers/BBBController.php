<?php

namespace App\Http\Controllers;

use App\Services\BBBService;
use Illuminate\Http\JsonResponse;

class BBBController extends Controller
{
    private BBBService $service;

    public function __construct()
    {
        $this->service = new BBBService();
    }

    /**
     * Tạo buổi meeting
     *
     * @param string $meetingName tên buổi học
     *
     * @return JsonResponse
     */
    public function createMeeting(string $meetingName): JsonResponse
    {
        $meetingName = urldecode($meetingName);

        return response()->json($this->service->createMeeting($meetingName));
    }

    /**
     * Join meeting
     *
     * @param string $meetingId meeting id
     * @param string $username  tên người dùng được hiển thị trong buổi meeting
     * @param string $password  truyền moderatorPW lấy từ API /bbb/create_meeting/{meetingName} để join với role
     *                          moderator truyền attendeePW lấy từ API /bbb/create_meeting/{meetingName} để join với
     *                          role attendee
     */
    public function joinMeeting(string $meetingId, string $username, string $password)
    {
        $username = urldecode($username);
        $url      = $this->service->getJoinMeetingURL($meetingId, $username, $password);

        // Join the meeting by redirecting the user to the generated URL
        header('Status: 301 Moved Permanently', false, 301);
        header('Location:' . $url);
        exit();
    }

}
